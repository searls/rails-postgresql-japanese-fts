class CreateItems < ActiveRecord::Migration[6.1]
  def change
    create_table :items do |t|
      t.text :name
      t.tsvector :name_tsvector
      t.text :meaning
      t.text :readings, array: true
      t.tsvector :readings_tsvector

      t.timestamps

      t.index :name_tsvector, using: "GIN"
      t.index "to_tsvector('english', meaning)", using: "GIN"
      t.index :readings_tsvector, using: "GIN"
    end
  end
end
