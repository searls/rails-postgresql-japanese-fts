class CreateTokens < ActiveRecord::Migration[6.1]
  def change
    create_table :tokens do |t|
      t.text :name, null: false, unique: true

      t.index "name text_pattern_ops", unique: true
    end
  end
end
