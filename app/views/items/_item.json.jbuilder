json.extract! item, :id, :name, :name_tsvector, :meaning, :readings, :readings_tsvector, :created_at, :updated_at
json.url item_url(item, format: :json)
