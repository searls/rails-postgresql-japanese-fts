# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

## Sample

```ruby
Item.create!(name: "日本人", meaning: "Japanese", readings: ["にほんじん"])
Item.create!(name: "日本", meaning: "Japan", readings: ["にほん", "にっぽん"])
Item.fts("本")
# [#<Item:0x0000556dbfc84100
#   id: 1,
#   name: "日本人",
#   name_tsvector: "'人':3 '日本':1 '本人':2",
#   meaning: "Japanese",
#   readings: ["にほんじん"],
#   readings_tsvector: "'じん':4 'にほ':1 'ほん':2 'ん':5 'んじ':3",
#   created_at: Thu, 24 Dec 2020 20:13:40.869535000 UTC +00:00,
#   updated_at: Thu, 24 Dec 2020 20:13:40.869535000 UTC +00:00>,
#  #<Item:0x0000556dbfc8ffa0
#   id: 2,
#   name: "日本",
#   name_tsvector: "'日本':1 '本':2",
#   meaning: "Japan",
#   readings: ["にほん", "にっぽん"],
#   readings_tsvector: "'っぽ':6 'にっ':5 'にほ':1 'ほん':2 'ぽん':7 'ん':38",
#   created_at: Thu, 24 Dec 2020 20:13:43.381229000 UTC +00:00,
#   updated_at: Thu, 24 Dec 2020 20:13:43.381229000 UTC +00:00>]
Item.fts("日本人")
# [#<Item:0x0000556dbfd4eec8
#   id: 1,
#   name: "日本人",
#   name_tsvector: "'人':3 '日本':1 '本人':2",
#   meaning: "Japanese",
#   readings: ["にほんじん"],
#   readings_tsvector: "'じん':4 'にほ':1 'ほん':2 'ん':5 'んじ':3",
#   created_at: Thu, 24 Dec 2020 20:13:40.869535000 UTC +00:00,
#   updated_at: Thu, 24 Dec 2020 20:13:40.869535000 UTC +00:00>]
Item.fts('にほん')
# [#<Item:0x0000556dbe4887b8
#   id: 1,
#   name: "日本人",
#   name_tsvector: "'人':3 '日本':1 '本人':2",
#   meaning: "Japanese",
#   readings: ["にほんじん"],
#   readings_tsvector: "'じん':4 'にほ':1 'ほん':2 'ん':5 'んじ':3",
#   created_at: Thu, 24 Dec 2020 20:13:40.869535000 UTC +00:00,
#   updated_at: Thu, 24 Dec 2020 20:13:40.869535000 UTC +00:00>,
#  #<Item:0x0000556dbe488538
#   id: 2,
#   name: "日本",
#   name_tsvector: "'日本':1 '本':2",
#   meaning: "Japan",
#   readings: ["にほん", "にっぽん"],
#   readings_tsvector: "'っぽ':6 'にっ':5 'にほ':1 'ほん':2 'ぽん':7 'ん':38",
#   created_at: Thu, 24 Dec 2020 20:13:43.381229000 UTC +00:00,
#   updated_at: Thu, 24 Dec 2020 20:13:43.381229000 UTC +00:00>]
Item.fts('Japanese')
# [#<Item:0x0000556dbfe3cd30
#   id: 1,
#   name: "日本人",
#   name_tsvector: "'人':3 '日本':1 '本人':2",
#   meaning: "Japanese",
#   readings: ["にほんじん"],
#   readings_tsvector: "'じん':4 'にほ':1 'ほん':2 'ん':5 'んじ':3",
#   created_at: Thu, 24 Dec 2020 20:13:40.869535000 UTC +00:00,
#   updated_at: Thu, 24 Dec 2020 20:13:40.869535000 UTC +00:00>]
```
